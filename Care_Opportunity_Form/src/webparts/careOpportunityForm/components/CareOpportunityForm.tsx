import * as React from 'react';
import styles from './CareOpportunityForm.module.scss';
import { ICareOpportunityFormProps } from './ICareOpportunityFormProps';
import { escape } from '@microsoft/sp-lodash-subset';
import { sp, IItemUpdateResult } from "@pnp/sp/presets/all";
import { Pivot, PivotItem, PivotLinkFormat } from 'office-ui-fabric-react/lib/Pivot';
import { IPersonaProps } from "office-ui-fabric-react/lib/components/Persona/Persona.types";
import { PeoplePicker} from "@pnp/spfx-controls-react/lib/PeoplePicker";
import { RichText } from "@pnp/spfx-controls-react/lib/RichText";
import { DatePicker, IDatePickerStrings, IIconProps, IconButton, Label, PrimaryButton, DialogType, DefaultButton, TextField, Dropdown, Spinner, IChoiceGroupOption, IPivotItemProps, IDropdownOption, values, ComboBox, IComboBoxOption, Dialog, DialogFooter, arraysEqual, List} from 'office-ui-fabric-react';
import { IStackTokens, Stack } from 'office-ui-fabric-react/lib/Stack';
import {TagPicker,ITag,IBasePickerSuggestionsProps} from 'office-ui-fabric-react/lib/Pickers';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
import { Icon } from 'office-ui-fabric-react/lib/Icon';
import "@pnp/sp/webs";
import "@pnp/sp/site-users/web";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "@pnp/sp/sputilities";
import { IEmailProperties } from "@pnp/sp/sputilities";
import { ListView, IViewField, SelectionMode, GroupOrder, IGrouping } from "@pnp/spfx-controls-react/lib/ListView";
import "@pnp/sp/webs";
import "@pnp/sp/site-users/web";
import "@pnp/sp/sites";

const stackTokens: IStackTokens = { childrenGap: 10 };

interface IRoomRates {
  dtaRateType: string;
  dtaSelfFunded: string;
  dtaLAFunded: string;
  dtaCostOfCare: string;
  dtaProposedWeeklyFee: string;
}
interface ISummaryData {
  dtaDateRecieved: Date;
  dtaOpportunityStatusKey: string;
  dtaCommissioners: ITag[];
  dtaContractingCompany: ITag[];
  dtaOpportunityName: string;
  dtaOpportunitySummary: string;
  dtaClarificationDeadline: Date;
  dtaSubmissionDeadline: Date;
  dtaContractStartDate: Date;
  dtaContractExpiryDate: Date;
  dtaOpportunityTypeKey: string;
  dtaSummaryStatement: string;
  opportunityStatusOptions: IComboBoxOption[];
  opportunityTypeOptions: IDropdownOption[];
}
interface IHomeDetails {
  dtaHome: ITag[];
  dtaTotalOccupancy: number;
  dtaTotalOccupancySF: string;
  dtaTotalOccupancyLA: string;
  dtaRoomRates: string;
  dtaRoomRatesSF: string;
  dtaRoomRatesLA: string;
  dtaCostOfCare: string;
  dtaAdditionalDetails: string;
  dtaTUPE: boolean;
  dtaTUPEAdditionalNotes: string;
  dtaPriceSubmissionRequired: boolean;
  dtaPriceAdditionalNotes: string;
  dtaMethodStatementsRequired: boolean;
  dtaMethodAdditionalNotes: string;
  dtaPositionTag: number;
  dtaWeeklyFee: string;
  dtaRoomRatesList: IRoomRates[];
}

interface IBackgroundExit {
  dtaContractTerms: string;
  dtaCareSpec: string;
  dtaExitArrangements: boolean;
  dtaTerminationClause: string;
  dtaExitIssues: string;
  dtaComplete: boolean;
}

interface IStakeholderReview {
  dtaDateSent: Date;
  dtaResponseDeadline: Date;
  dtaReminders: boolean;
  dtaDays: string;
  dtaHeadCommAnalysis: IPersonaProps[];
  dtaHeadOfSales: IPersonaProps[];
  dtaRegionalDirector: IPersonaProps[];
  dtaRegionalManager: IPersonaProps[];
  dtaHeadCommAnalysisNotes: string;
  dtaHeadOfSalesNotes: string;
  dtaRegionalDirectorNotes: string;
  dtaRegionalManagerNotes: string;
  dtaHeadCommAnalysisEndorsement: string;
  dtaHeadOfSalesEndorsement: string;
  dtaRegionalDirectorEndorsement: string;
  dtaRegionalManagerEndorsement: string;
  dtaComplete: boolean;
}

interface IDirectorReview {
  dtaSustainabilityMatrix: string;
  dtaSupportingViewsInput: string;
  dtaFinanceKey: string;
  dtaReccomendation: string;
  dtaGroupDirector: IPersonaProps[];
  dtaDirectorBusinessDev: IPersonaProps[];
  dtaDirectorOperations: IPersonaProps[];
  dtaDirectorFinance: IPersonaProps[];
  dtaGroupDirectorAuthorised: string;
  dtaDirectorBusinessDevAuthorised: string;
  dtaDirectorOperationsAuthorised: string;
  dtaDirectorFinanceAuthorised: string;
  dtaGroupDirectorNotes: string;
  dtaDirectorBusinessDevNotes: string;
  dtaDirectorOperationsNotes: string;
  dtaDirectorFinanceNotes: string;
  dtaComplete: boolean;
  dtaResponseDeadline: Date;
  dtaDateSent: Date;
  dtaDirectorReminders: boolean;
  dtaReminderInterval: string;
}

const DayPickerStrings: IDatePickerStrings = {
  months: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  shortDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
  goToToday: 'Go to today',
  prevMonthAriaLabel: 'Go to previous month',
  nextMonthAriaLabel: 'Go to next month',
  prevYearAriaLabel: 'Go to previous year',
  nextYearAriaLabel: 'Go to next year',
  closeButtonAriaLabel: 'Close date picker'
};

interface IDocumentList {
  documentName: string;
  dateAdded: Date;
  relativeURL: string;
  documentType: string;
  status: string;
}

const CareOpportunityForm = (props) => {
  const [dtaSummary, setDtaSummary] = React.useState<ISummaryData>({opportunityTypeOptions:[], opportunityStatusOptions: [], dtaSummaryStatement: "", dtaOpportunityStatusKey: "Opportunity Being Prepared", dtaClarificationDeadline: null, dtaCommissioners: [], dtaContractExpiryDate: null, dtaContractStartDate: null, dtaContractingCompany: [], dtaDateRecieved: null, dtaOpportunityName: "", dtaOpportunitySummary: "", dtaOpportunityTypeKey: "", dtaSubmissionDeadline: null});
  const [selectedTabOpportunity, setSelectedTabOpportunity] = React.useState(0);
  const [selectedMainTab, setSelectedMainTab] = React.useState(0);
  const [dtaHomeDetails, setDtaHomeDetails] = React.useState<IHomeDetails[]>([{dtaWeeklyFee: null, dtaPositionTag: -1, dtaAdditionalDetails: "", dtaCostOfCare: "", dtaHome: [], dtaMethodAdditionalNotes: "", dtaMethodStatementsRequired: null, dtaPriceAdditionalNotes: "", dtaPriceSubmissionRequired: null, dtaRoomRates: "", dtaRoomRatesLA: "", dtaRoomRatesSF: "", dtaTUPE: null, dtaTUPEAdditionalNotes: "", dtaTotalOccupancy: 0, dtaTotalOccupancyLA: "", dtaTotalOccupancySF: "", dtaRoomRatesList: [{dtaCostOfCare: "", dtaLAFunded: "", dtaProposedWeeklyFee: "", dtaRateType: "", dtaSelfFunded: ""}]}]);
  const [homesDataPicker, setHomesDataPicker] = React.useState<ITag[]>([]);
  const [dtaBackgroundExit, setDtaBackgroundExit] = React.useState<IBackgroundExit>({dtaComplete: false, dtaCareSpec: "", dtaContractTerms: "", dtaExitArrangements: null, dtaExitIssues: "", dtaTerminationClause: "" });
  const [dtaStakeholderReview, setDtaStakeholderReview] = React.useState<IStakeholderReview>({dtaComplete: false, dtaDateSent: null, dtaDays: "", dtaHeadCommAnalysis: [], dtaHeadCommAnalysisNotes: "", dtaHeadOfSales: [], dtaHeadOfSalesNotes: "", dtaRegionalDirector: [], dtaRegionalDirectorNotes: "", dtaRegionalManager: [], dtaRegionalManagerNotes: "", dtaReminders: null, dtaResponseDeadline: null, dtaRegionalManagerEndorsement: "", dtaRegionalDirectorEndorsement: "", dtaHeadOfSalesEndorsement: "", dtaHeadCommAnalysisEndorsement: ""});
  const [CommissionerDataPicker, setCommissionerDataPicker] = React.useState<ITag[]>([]);
  const [dtaDirectorReview, setDtaDirectorReview] = React.useState<IDirectorReview>({dtaDateSent: null, dtaDirectorReminders: false, dtaReminderInterval: "", dtaResponseDeadline: null,  dtaComplete: false, dtaDirectorBusinessDev: [], dtaDirectorBusinessDevAuthorised: "", dtaDirectorBusinessDevNotes: "", dtaDirectorFinance: [], dtaDirectorFinanceAuthorised: "", dtaDirectorFinanceNotes: "", dtaDirectorOperations: [], dtaDirectorOperationsAuthorised: "", dtaDirectorOperationsNotes: "", dtaFinanceKey: "", dtaGroupDirector: [], dtaReccomendation: "", dtaGroupDirectorAuthorised: "", dtaGroupDirectorNotes: "", dtaSupportingViewsInput: "", dtaSustainabilityMatrix: ""});
  const [contractingCompanyPicker, setcontractingCompanyPicker] = React.useState<ITag[]>([]);
  const editMode = React.useRef<boolean>(false);
  const currentID = React.useRef<number>(-1);
  const dependenciesLoaded = React.useRef<boolean>(false);
  const formData = React.useRef(null);
  const mismatchFound = React.useRef<boolean>(null);
  const [formLoaded, setFormLoaded] = React.useState<boolean>(true);
  const sendStakeholderEmail = React.useRef(false);
  const sendDirectorEmail = React.useRef(false);
  const stakeholderReviewDone = React.useRef(false);
  const contract_team_email_Status = React.useRef("Not Sent");
  const [documentLibrary, getDocumentLibrary] = React.useState([]);
  const [dtaDocumentList, setDtaDocumentList] = React.useState<IDocumentList[]>([]);
  const [areYouSureStakeholders, setAreYouSureStakeholders] = React.useState<boolean>(false);
  const [areYouSureDirectors, setareYouSureDirectors] = React.useState<boolean>(false);
  const [saving, setSaving] = React.useState(false);
  const [ignored, forceUpdate] = React.useReducer(x => x + 1, 0);

  const getRelatedDocuments = async (ID: number) => {
    // Pull list items filtered to current ID
    let documentLibraryList = await sp.web.lists.getById("1ce68be7-7b4f-4ae5-ae19-eec45fa80f6d").items.get();
    documentLibraryList = await sp.web.lists.getById("1ce68be7-7b4f-4ae5-ae19-eec45fa80f6d").items.filter(`Opportunity_x0020_TitleId eq ${ID}`).get();
    let processedList = await Promise.all(documentLibraryList.map(async (item) => {
     let relatedDoc = await sp.web.lists.getById("1ce68be7-7b4f-4ae5-ae19-eec45fa80f6d").items.getById(item.Id).file();
     return {documentName: relatedDoc.Name, dateAdded: new Date(item.Created), relativeURL: `https://${window.location.hostname}${relatedDoc.ServerRelativeUrl}`, documentType: item.Document_x0020_Type, status: item.TenderStatus};
    }));
    setDtaDocumentList(processedList);
  };

  const colourPick = (text: string) => {
    if(text == "Completed") {
      return styles.colorGreen;
    }
    else if(text == "Action Required") {
      return styles.colorRed;
    }
    else {
      return styles.noColour;
    }
  };


  const viewFields: IViewField[] = [{
      name: 'documentName',
      displayName: 'File Name',
      isResizable: true,
      render: (rowitem) => {
        return (<a href={`${rowitem.relativeURL}`} target="_blank" >{rowitem.documentName}</a>);
    },
    },
    {
      name: 'status',
      displayName: 'Status',
      isResizable: true,
      minWidth: 200,
      render: (rowitem) => {
      return (<div className={colourPick(rowitem.status)}>{rowitem.status}</div>);
      },
    },
    {
      name: 'documentType',
      displayName: 'Document Type',
      isResizable: true,
      minWidth: 100
    },
  ];

  const fileDropped = async (file: File[]) => {
    if(file.length > 1) {
      alert("Please only drag in 1 file at a time");
    } else if(file.length == 1) {
      let response = await sp.web.getFolderByServerRelativeUrl("/sites/CARE-TEST-Contracts/Care Contract Opportunities Document Library/").files.add(file[0].name, file[0]);
      console.log(file[0]);
      let item = await response.file.getItem();
      let final = await item.update({
        Opportunity_x0020_TitleId: currentID.current,
        Document_x0020_Type: "Contract",
        TenderStatus: "Action Required"
      });
      alert("Uploaded Successfully!");
      getRelatedDocuments(currentID.current);
    }
  };

  const newHome = () => {
    let updatedHome = [...dtaHomeDetails];
    updatedHome.push({dtaWeeklyFee: null, dtaPositionTag: -1, dtaAdditionalDetails: "", dtaCostOfCare: "", dtaHome: [], dtaMethodAdditionalNotes: "", dtaMethodStatementsRequired: null, dtaPriceAdditionalNotes: "", dtaPriceSubmissionRequired: null, dtaRoomRates: "", dtaRoomRatesLA: "", dtaRoomRatesSF: "", dtaTUPE: null, dtaTUPEAdditionalNotes: "", dtaTotalOccupancy: 0, dtaTotalOccupancyLA: "", dtaTotalOccupancySF: "", dtaRoomRatesList: [{dtaCostOfCare: "", dtaLAFunded: "", dtaProposedWeeklyFee: "", dtaRateType: "", dtaSelfFunded: ""}]});
    setDtaHomeDetails(updatedHome);
  };

  const removeHome = () => {
    let updatedHome: IHomeDetails[] = [];
    if(dtaHomeDetails.length > 1) {
      updatedHome = dtaHomeDetails;
      updatedHome.pop();
      setDtaHomeDetails([...updatedHome]);
    } else {
      setDtaHomeDetails(dtaHomeDetails);
    }
  };

  const getHomes = async (recordID: number) => {
    let homeData = await sp.web.lists.getById("626f9e40-527d-472a-80a9-4ee6640ff524").items.filter(`TenderId eq ${recordID}`).getAll();
    let processedHomeData: IHomeDetails[] = [];
    homeData.forEach(async (home) => {
      let returnData: IHomeDetails;
      let homeRecord: any;
      let homeName: string;
      let totalOccupancy: number = 0;
      if(home.HomeId) {
        homeRecord = await sp.web.lists.getById("43fb1e67-1e38-446d-876e-e83db983aa1b").items.filter(`Id eq ${home.HomeId}`).get();
        if(homeRecord.length > 0) {
          homeName = homeRecord[0].Title;
          totalOccupancy = homeRecord[0].Total_x0020_Occupancy;
        } else {
          alert("Error: One of the selected homes no longer exists on the SharePoint list!");
        }
      }
      console.log(`Total Occupancy: ${home.totalOccupancy}`);
      returnData = {
        ...returnData,
        dtaHome: homeName != null? [{key: home.HomeId, name: homeName}]: [],
        dtaTotalOccupancySF: home.Self_Funded_Occ,
        dtaRoomRatesSF: home.Self_Funded_Rate,
        dtaRoomRatesLA: home.LA_Funded_Rate,
        dtaCostOfCare: home.Cost_Of_Care,
        dtaAdditionalDetails: home.Additional_Details,
        dtaTUPE: home.TUPE,
        dtaPriceSubmissionRequired: home.Price_Submission,
        dtaMethodStatementsRequired: home.Method_Statements,
        dtaTUPEAdditionalNotes: home.TUPE_Notes,
        dtaPriceAdditionalNotes: home.Price_Submission_Notes,
        dtaMethodAdditionalNotes: home.Method_Statements_Notes,
        dtaTotalOccupancy: home.totalOccupancy != null? home.totalOccupancy : 0,
        dtaPositionTag: home.HomeOrder,
        dtaTotalOccupancyLA: home.LA_Funded_Occ,
        dtaWeeklyFee: home.proposed_weekly_fee,
        dtaRoomRatesList: home.rates_options != null ? JSON.parse(home.rates_options) : [{dtaCostOfCare: "", dtaLAFunded: "", dtaProposedWeeklyFee: "", dtaRateType: "", dtaSelfFunded: ""}]
      };
      processedHomeData.push(returnData);
    });
    setDtaHomeDetails(processedHomeData);
    setFormLoaded(true);
  };

  async function checkEditMode() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const EditModeId: string = urlParams.get('EditModeId');
    let data: any;

    if(EditModeId) {
      // URL param there means user wants edit mode
      // Fetch data..
      if(isNaN(Number(EditModeId))) {
        alert("Error: URL parameter is invalid. Please contact your administrator if this problem persists.");
      }
      else {
        currentID.current = Number(EditModeId);
        editMode.current = true;
        // Attempt to load the data
        getRelatedDocuments(currentID.current);

        formData.current = await sp.web.lists.getById("eff099e9-fa68-4675-9712-54ff99e68385").items.filter(`Id eq ${EditModeId}`).get();
        if(formData.current.length === 0) {
          alert("Error trying to load the record");
        }
        else {
          setFormLoaded(false);
          let AuthorityID = CommissionerDataPicker.map((val) => val.key).indexOf(formData.current[0].CommissionersId);
          let ContractingCompanyID = contractingCompanyPicker.map((val) => val.key).indexOf(formData.current[0].contracting_companyId);
          data = formData.current[0];
          contract_team_email_Status.current = data.contractTeamEmail;
          // Found record, populate values and trigger re-render
          setDtaSummary({
            ...dtaSummary,
            // You need to check if the value is null else the generated date for new Date(null) will be 01/01/1970 which will then be saved to the list!!
            dtaDateRecieved: data.Date_x0020_Received != null ? new Date(data.Date_x0020_Received): null,
            dtaCommissioners: AuthorityID === -1 ? null : [CommissionerDataPicker[AuthorityID]],
            dtaContractingCompany: ContractingCompanyID === -1 ? null : [contractingCompanyPicker[ContractingCompanyID]],
            dtaOpportunityTypeKey: data.Opportunity_x0020_Type,
            dtaOpportunityName: data.Title,
            dtaOpportunitySummary: data.Opportunity_x0020_Summary,
            dtaClarificationDeadline: data.Clarification_x0020_Deadline != null ?  new Date(data.Clarification_x0020_Deadline): null,
            dtaSubmissionDeadline: data.Submission_x0020_Deadline != null ? new Date(data.Submission_x0020_Deadline): null,
            dtaContractStartDate: data.Contract_x0020_Start_x0020_Date != null ? new Date(data.Contract_x0020_Start_x0020_Date): null,
            dtaContractExpiryDate: data.Contract_x0020_End_x0020_Date != null ? new Date(data.Contract_x0020_End_x0020_Date): null,
            dtaOpportunityStatusKey: data.workflow_status,
            dtaSummaryStatement: data.Summary_Statement,
          });
          setDtaBackgroundExit({
            ...dtaBackgroundExit,
            dtaContractTerms: data.Notable_x0020_Contract_x0020_Ter,
            dtaCareSpec: data.Care_x0020_Specification,
            dtaExitArrangements: data.Exit_x0020_Arrangements_x0020_Re,
            dtaTerminationClause: data.Termination_x0020_Clause,
            dtaExitIssues: data.Detail_x0020_Possible_x0020_Exit,
            dtaComplete: data.section1complete,
          });

          // Need to get the emails for the people in the data.
          let headCommAnalysisEmail: string = null;
          let headSalesEmail: string = null;
          let regional_Director: string = null;
          let regional_Manager: string = null;
          if(data.head_comm_analysisId) headCommAnalysisEmail = (await sp.web.siteUsers.getById(data.head_comm_analysisId).get()).Email;
          if(data.head_salesId) headSalesEmail = (await sp.web.siteUsers.getById(data.head_salesId).get()).Email;
          if(data.regional_directorId) regional_Director =  (await sp.web.siteUsers.getById(data.regional_directorId).get()).Email;
          if(data.regional_managerId) regional_Manager =  (await sp.web.siteUsers.getById(data.regional_managerId).get()).Email;

          // Has the stakeholder reivew already happened?
          stakeholderReviewDone.current = data.stakeholder_review_done;

          setDtaStakeholderReview({
            ...dtaStakeholderReview,
            dtaDateSent: data.date_sent != null ?  new Date(data.date_sent): null,
            dtaResponseDeadline: data.response_deadline != null ?  new Date(data.response_deadline): null,
            dtaReminders: data.reminders,
            dtaDays: data.days,
            dtaHeadCommAnalysis: headCommAnalysisEmail != null ? [{secondaryText: headCommAnalysisEmail}]: [],
            dtaHeadOfSales: headSalesEmail != null ? [{secondaryText: headSalesEmail}]: [],
            dtaRegionalDirector: regional_Manager != null ?  [{secondaryText: regional_Director}]: [],
            dtaRegionalManager: regional_Manager != null ? [{secondaryText: regional_Manager}]: [],
            dtaHeadCommAnalysisEndorsement: data.head_comm_analysis_endorsed,
            dtaHeadOfSalesEndorsement: data.head_sales_endorsed,
            dtaRegionalDirectorEndorsement: data.regional_director_endorsed,
            dtaRegionalManagerEndorsement: data.regional_manager_endorsed,
            dtaHeadCommAnalysisNotes: data.head_comm_analysis_notes,
            dtaHeadOfSalesNotes: data.head_sales_notes,
            dtaRegionalDirectorNotes: data.regional_director_notes,
            dtaRegionalManagerNotes: data.regional_manager_notes,
            dtaComplete: data.section2complete,
          });

          let groupDirector: string = null;
          let businessDevelopment: string = null;
          let directorOperations: string = null;
          let directorFinance: string = null;
          if(data.group_directorId) groupDirector = (await sp.web.siteUsers.getById(data.group_directorId).get()).Email;
          if(data.business_development_directorId) businessDevelopment = (await sp.web.siteUsers.getById(data.business_development_directorId).get()).Email;
          if(data.director_operationsId) directorOperations =  (await sp.web.siteUsers.getById(data.director_operationsId).get()).Email;
          if(data.director_financeId) directorFinance =  (await sp.web.siteUsers.getById(data.director_financeId).get()).Email;

          setDtaDirectorReview({
            ...dtaDirectorReview,
            dtaSustainabilityMatrix: data.Sustainability_Matrix,
            dtaSupportingViewsInput: data.supporting_views_input,
            dtaFinanceKey: data.Recommendation,
            dtaReccomendation: data.director_review_notes,
            dtaGroupDirectorAuthorised: data.group_director_authorised,
            dtaDirectorBusinessDevAuthorised: data.business_development_authorised,
            dtaDirectorOperationsAuthorised: data.director_operations_authorised,
            dtaDirectorFinanceAuthorised: data.director_finance_authorised,
            dtaGroupDirectorNotes: data.group_director_notes,
            dtaDirectorBusinessDevNotes: data.business_development_notes,
            dtaDirectorOperationsNotes: data.director_operations_notes,
            dtaDirectorFinanceNotes: data.director_finance_notes,
            dtaGroupDirector: groupDirector != null ? [{secondaryText: groupDirector}]: [],
            dtaDirectorBusinessDev: businessDevelopment != null ? [{secondaryText: businessDevelopment}]: [],
            dtaDirectorFinance: directorFinance != null ? [{secondaryText: directorFinance}]: [],
            dtaDirectorOperations: directorOperations != null ? [{secondaryText: directorOperations}]: [],
            dtaComplete: data.section3complete,
            dtaDateSent: data.Director_Date_Sent != null ? new Date(data.Director_Date_Sent): null,
            dtaResponseDeadline: data.Director_Response_Deadline != null ? new Date(data.Director_Response_Deadline): null,
            dtaDirectorReminders: data.Director_Reminders,
            dtaReminderInterval: data.Reminder_Interval
          });
          getHomes(data.Id);
        }
      }
    }
  }

  const checkMergeError: any = async () => {
   // This function will send a query to the SharePoint list and compare the returned data to the data currently stored to ensure no discrepancy
   let errorMsg: string[] = [];
   let returnVal = false;
   if(editMode.current) {
    let latestData = await sp.web.lists.getById("eff099e9-fa68-4675-9712-54ff99e68385").items.filter(`Id eq ${currentID.current}`).get();
    if(latestData.length > 0) {
     let latest = latestData[0];
     let previous = formData.current[0];
     // Summary tests
       if(latest.Date_x0020_Received !== previous.Date_x0020_Received)
         errorMsg.push(`Date Recieved: ${new Date(latest.Date_x0020_Received).toDateString()}`);
       if(latest.CommissionersId !== previous.CommissionersId)
         errorMsg.push("Commissioner has been modified");
       if(latest.contracting_companyId !== previous.contracting_companyId)
         errorMsg.push("Contracting Company has been modified");
       if(latest.Opportunity_x0020_Type !== previous.Opportunity_x0020_Type)
         errorMsg.push(`Opportunity type has been changed to: ${latest.Opportunity_x0020_Type}`);
       if(latest.Title !== previous.Title)
         errorMsg.push(`Opportunity Name has been changed to: ${latest.Title}`);
       if(latest.Opportunity_x0020_Summary !== previous.Opportunity_x0020_Summary)
         errorMsg.push("Opportunity Summary has been modified");
       if(latest.Clarification_x0020_Deadline !== previous.Clarification_x0020_Deadline)
         errorMsg.push(`Clarification Deadline has been changed to: ${new Date(latest.Clarification_x0020_Deadline).toDateString()}`);
       if(latest.Submission_x0020_Deadline !== previous.Submission_x0020_Deadline)
         errorMsg.push(`Submission Deadline has been changed to: ${new Date(latest.Submission_x0020_Deadline).toDateString()}`);
       if(latest.Contract_x0020_Start_x0020_Date !== previous.Contract_x0020_Start_x0020_Date)
         errorMsg.push(`Contract Start Date has been changed to: ${new Date(latest.Contract_x0020_Start_x0020_Date).toDateString()}`);
       if(latest.Contract_x0020_End_x0020_Date !== previous.Contract_x0020_End_x0020_Date)
         errorMsg.push(`Contract End Date has been changed to: ${new Date(latest.Contract_x0020_End_x0020_Date).toDateString()}`);
       if(latest.workflow_status !== previous.workflow_status)
         errorMsg.push(`Workflow Status has been changed to ${latest.workflow_status}`);
 
       // Background and Exit test
       if(latest.Notable_x0020_Contract_x0020_Ter !== previous.Notable_x0020_Contract_x0020_Ter)
         errorMsg.push(`Notable Contract Terms have been changed`);
       if(latest.Care_x0020_Specification !== previous.Care_x0020_Specification)
         errorMsg.push("Care specification has been modified");
       if(latest.Exit_x0020_Arrangements_x0020_Re !== previous.Exit_x0020_Arrangements_x0020_Re)
         errorMsg.push(`Exit requirements has been changed`);
       if(latest.Termination_x0020_Clause !== previous.Termination_x0020_Clause)
         errorMsg.push(`Termination Clause has been changed`);
       if(latest.Detail_x0020_Possible_x0020_Exit !== previous.Detail_x0020_Possible_x0020_Exit)
         errorMsg.push(`Detail Possible Exit Issues has been changed`);
 
       // Stakeholder review
       if(latest.date_sent !== previous.date_sent)
         errorMsg.push(`Date sent has been changed to ${new Date(latest.date_sent).toDateString()}`);
       if(latest.response_deadline !== previous.response_deadline)
         errorMsg.push(`Response Deadline has been changed to ${new Date(latest.response_deadline).toDateString()}`);
       if(latest.reminders !== previous.reminders)
         errorMsg.push(`Reminders has been changed`);
       if(latest.days !== previous.days)
         errorMsg.push(`Days has been changed to ${String(latest.days)}`);
       if(latest.head_comm_analysisId !== previous.head_comm_analysisId)
         errorMsg.push(`Head of Commercial Analysis has changed`);
       if(latest.head_salesId !== previous.head_salesId)
         errorMsg.push(`Head of Sales and Marketing has been changed`);
       if(latest.regional_directorId !== previous.regional_directorId)
         errorMsg.push(`Regional Director has been changed`);
       if(latest.regional_managerId !== previous.regional_managerId)
         errorMsg.push(`Regional manager has been changed`);
       if(latest.head_comm_analysis_endorsed !== previous.head_comm_analysis_endorsed)
         errorMsg.push(`Regional Manager endorsement has changed`);
       if(latest.head_sales_endorsed !== previous.head_sales_endorsed)
         errorMsg.push(`Head of Sales and Marketing endorsement has changed`);
       if(latest.regional_director_endorsed !== previous.regional_director_endorsed)
         errorMsg.push(`Regional Director endorsement has changed`);
       if(latest.regional_manager_endorsed !== previous.regional_manager_endorsed)
         errorMsg.push(`Regional Manager endorsement has changed`);
       if(latest.head_comm_analysis_notes !== previous.head_comm_analysis_notes)
         errorMsg.push(`Head of Commercial Analysis notes has changed`);
       if(latest.head_sales_notes !== previous.head_sales_notes)
         errorMsg.push(`Head of Sales and Marketing notes has changed`);
       if(latest.regional_director_notes !== previous.regional_director_notes)
         errorMsg.push(`Regional Director notes has changed`);
       if(latest.regional_manager_notes !== previous.regional_manager_notes)
         errorMsg.push(`Regional Manager notes has changed`);
 
       // Director review
       if(latest.Sustainability_Matrix !== previous.Sustainability_Matrix)
         errorMsg.push(`Sustainability Matrix has changed`);
       if(latest.supporting_views_input !== previous.supporting_views_input)
         errorMsg.push(`Supporting Views Input has changed`);
       if(latest.Recommendation !== previous.Recommendation)
         errorMsg.push(`Recommendation has changed`);
       if(latest.director_review_notes !== previous.director_review_notes)
         errorMsg.push(`Director notes has changed`);
       if(latest.group_directorId !== previous.group_directorId)
         errorMsg.push(`Group Director has changed`);
       if(latest.business_development_directorId !== previous.business_development_directorId)
         errorMsg.push(`Business Development Director has been changed`);
       if(latest.director_operationsId !== previous.director_operationsId)
         errorMsg.push(`Director of Operations has changed`);
       if(latest.director_financeId !== previous.director_financeId)
         errorMsg.push(`Director of Finance has changed`);
       if(latest.group_director_authorised !== previous.group_director_authorised)
         errorMsg.push(`Group Director authorisation has changed`);
 
       if(latest.business_development_authorised !== previous.business_development_authorised)
         errorMsg.push(`Business Development Director authorisation has changed`);
       if(latest.director_operations_authorised !== previous.director_operations_authorised)
         errorMsg.push(`Director of Operations authorisation has changed`);
       if(latest.director_finance_authorised !== previous.director_finance_authorised)
         errorMsg.push(`Director of Finance authorisation has changed`);
       if(latest.group_director_notes !== previous.group_director_notes)
         errorMsg.push(`Group Director notes has changed`);
       if(latest.business_development_notes !== previous.business_development_notes)
         errorMsg.push(`Business Development Director notes has changed`);
       if(latest.director_operations_notes !== previous.director_operations_notes)
         errorMsg.push(`Director of Operations notes has changed`);
       if(latest.director_finance_notes !== previous.director_finance_notes)
         errorMsg.push(`Director of Finance notes has changed`);
       if(errorMsg.length > 0) {
         alert(`Warning! Between you opening this form and saving, somebody else has modified the data. This means that you are about to overwrite what they changed.\nSave again to overwrite. They changed the following:\n${errorMsg.join("\n")}` );
         returnVal = true;
       }
       // Display this to the user
    } else {
      alert("The record that you are editing no longer exists on the SharePoint list!");
    }
 
   }
   mismatchFound.current = returnVal;
   return returnVal;
  };

  function OpportunityCompleteRenderer(link: IPivotItemProps, defaultRenderer: (link: IPivotItemProps) => JSX.Element, tabType: string): JSX.Element {
    let completeIcon = null;
    if(tabType === "Opportunity") {
      completeIcon = dtaBackgroundExit.dtaComplete ? <Icon iconName="Completed" style={{color: 'LightSeaGreen'}}/> : null;
    } else if(tabType === "Stakeholder") {
      completeIcon = dtaStakeholderReview.dtaComplete ? <Icon iconName="Completed" style={{color: 'LightSeaGreen'}}/> : null;
    } else if(tabType === "DirectorReview") {
      completeIcon = dtaDirectorReview.dtaComplete ? <Icon iconName="Completed" style={{color: 'LightSeaGreen'}}/> : null;
    }
    return (
      <Stack horizontal tokens={stackTokens}>
        {defaultRenderer(link)}
        {completeIcon}
      </Stack>
    );
  }

  const checkMandatory = () => {
    if(dtaSummary.dtaCommissioners.length > 0
      && dtaSummary.dtaContractingCompany.length > 0
      && dtaSummary.dtaOpportunityName.length > 0
      && dtaSummary.dtaOpportunitySummary.length > 0) {
        return true;
      } else {
        alert("Mandatory fields missing information");
        return false;
      }
  };

  const saveFormCallback = async (passive?: boolean) => {
    console.log("Saving!");
    let Head_Comm_Analysis: any = null;
    let Head_Of_Sales: any = null;
    let Regional_Director: any = null;
    let Regional_Manager: any = null;
    let Group_Director: any = null;
    let Business_Development: any = null;
    let Director_Operations: any = null;
    let Director_Finance: any = null;

    if(dtaStakeholderReview.dtaHeadCommAnalysis.length >= 1) {
      Head_Comm_Analysis = (await sp.web.siteUsers.getByEmail(dtaStakeholderReview.dtaHeadCommAnalysis[0].secondaryText).get()).Id;
    }
    if(dtaStakeholderReview.dtaHeadOfSales.length >= 1) {
      Head_Of_Sales = (await sp.web.siteUsers.getByEmail(dtaStakeholderReview.dtaHeadOfSales[0].secondaryText).get()).Id;
    }
    if(dtaStakeholderReview.dtaRegionalDirector.length >= 1) {
      Regional_Director = (await sp.web.siteUsers.getByEmail(dtaStakeholderReview.dtaRegionalDirector[0].secondaryText).get()).Id;
    }
    if(dtaStakeholderReview.dtaRegionalManager.length >= 1) {
      Regional_Manager = (await sp.web.siteUsers.getByEmail(dtaStakeholderReview.dtaRegionalManager[0].secondaryText).get()).Id;
    }
    if(dtaDirectorReview.dtaGroupDirector.length >= 1) {
      Group_Director = (await sp.web.siteUsers.getByEmail(dtaDirectorReview.dtaGroupDirector[0].secondaryText).get()).Id;
    }
    if(dtaDirectorReview.dtaDirectorBusinessDev.length >= 1) {
      Business_Development = (await sp.web.siteUsers.getByEmail(dtaDirectorReview.dtaDirectorBusinessDev[0].secondaryText).get()).Id;
    }
    if(dtaDirectorReview.dtaDirectorOperations.length >= 1) {
      Director_Operations = (await sp.web.siteUsers.getByEmail(dtaDirectorReview.dtaDirectorOperations[0].secondaryText).get()).Id;
    }
    if(dtaDirectorReview.dtaDirectorFinance.length >= 1) {
      Director_Finance = (await sp.web.siteUsers.getByEmail(dtaDirectorReview.dtaDirectorFinance[0].secondaryText).get()).Id;
    }

    let postData: any = {
      Date_x0020_Received: dtaSummary.dtaDateRecieved,
      CommissionersId: dtaSummary.dtaCommissioners.length > 0 ? dtaSummary.dtaCommissioners[0].key : null,
      contracting_companyId: dtaSummary.dtaContractingCompany.length > 0 ? dtaSummary.dtaContractingCompany[0].key : null,
      Opportunity_x0020_Type: dtaSummary.dtaOpportunityTypeKey,
      Title: dtaSummary.dtaOpportunityName,
      Opportunity_x0020_Summary: dtaSummary.dtaOpportunitySummary,
      Clarification_x0020_Deadline: dtaSummary.dtaClarificationDeadline,
      Submission_x0020_Deadline: dtaSummary.dtaSubmissionDeadline,
      Contract_x0020_Start_x0020_Date: dtaSummary.dtaContractStartDate,
      Contract_x0020_End_x0020_Date: dtaSummary.dtaContractExpiryDate,
      workflow_status: dtaSummary.dtaOpportunityStatusKey,
      Summary_Statement: dtaSummary.dtaSummaryStatement,

      // Background and exit
      Notable_x0020_Contract_x0020_Ter: dtaBackgroundExit.dtaContractTerms,
      Care_x0020_Specification: dtaBackgroundExit.dtaCareSpec,
      Exit_x0020_Arrangements_x0020_Re: dtaBackgroundExit.dtaExitArrangements,
      Termination_x0020_Clause: dtaBackgroundExit.dtaTerminationClause,
      Detail_x0020_Possible_x0020_Exit: dtaBackgroundExit.dtaExitIssues,
      section1complete: dtaBackgroundExit.dtaComplete,

      // Stakeholder review
      date_sent: dtaStakeholderReview.dtaDateSent,
      response_deadline: dtaStakeholderReview.dtaResponseDeadline,
      reminders: dtaStakeholderReview.dtaReminders,
      days: dtaStakeholderReview.dtaDays,
      head_comm_analysisId: Head_Comm_Analysis,
      head_salesId: Head_Of_Sales,
      regional_directorId: Regional_Director,
      regional_managerId: Regional_Manager,
      head_comm_analysis_endorsed: dtaStakeholderReview.dtaHeadCommAnalysisEndorsement,
      head_sales_endorsed: dtaStakeholderReview.dtaHeadOfSalesEndorsement,
      regional_director_endorsed: dtaStakeholderReview.dtaRegionalDirectorEndorsement,
      regional_manager_endorsed: dtaStakeholderReview.dtaRegionalManagerEndorsement,
      head_comm_analysis_notes: dtaStakeholderReview.dtaHeadCommAnalysisNotes,
      head_sales_notes: dtaStakeholderReview.dtaHeadOfSalesNotes,
      regional_director_notes: dtaStakeholderReview.dtaRegionalDirectorNotes,
      regional_manager_notes: dtaStakeholderReview.dtaRegionalManagerNotes,
      section2complete: dtaStakeholderReview.dtaComplete,

      // Director Review
      Sustainability_Matrix: dtaDirectorReview.dtaSustainabilityMatrix,
      supporting_views_input: dtaDirectorReview.dtaSupportingViewsInput,
      Recommendation: dtaDirectorReview.dtaFinanceKey,
      director_review_notes: dtaDirectorReview.dtaReccomendation,
      group_directorId: Group_Director,
      business_development_directorId: Business_Development,
      director_operationsId: Director_Operations,
      director_financeId: Director_Finance,
      group_director_authorised: dtaDirectorReview.dtaGroupDirectorAuthorised,
      business_development_authorised: dtaDirectorReview.dtaDirectorBusinessDevAuthorised,
      director_operations_authorised: dtaDirectorReview.dtaDirectorOperationsAuthorised,
      director_finance_authorised: dtaDirectorReview.dtaDirectorFinanceAuthorised,
      group_director_notes: dtaDirectorReview.dtaGroupDirectorNotes,
      business_development_notes: dtaDirectorReview.dtaDirectorBusinessDevNotes,
      director_operations_notes: dtaDirectorReview.dtaDirectorOperationsNotes,
      director_finance_notes: dtaDirectorReview.dtaDirectorFinanceNotes,
      section3complete: dtaDirectorReview.dtaComplete,
      contractTeamEmail: contract_team_email_Status.current,
      Director_Date_Sent: dtaDirectorReview.dtaDateSent,
      Director_Response_Deadline: dtaDirectorReview.dtaResponseDeadline,
      Director_Reminders: dtaDirectorReview.dtaDirectorReminders,
      Reminder_Interval: dtaDirectorReview.dtaReminderInterval
    };

    if(editMode.current === false) {
      sp.web.lists.getById("eff099e9-fa68-4675-9712-54ff99e68385").items.add(postData).then((returnedVal) => {
        console.log(returnedVal);
        sp.web.lists.getById("eff099e9-fa68-4675-9712-54ff99e68385").items.getById(returnedVal.data.Id).update({
          spfx_Edit_Link: {
            '__metadata': { 'type': 'SP.FieldUrlValue' },
            'Description': 'Click to edit item',
            'Url': `${window.location.href}?EditModeId=${returnedVal.data.Id}`
          }
        }).then(async () => {
          let promiseCount = 0;
          await new Promise((resolve) => {
            dtaHomeDetails.forEach(async (home, index) => {
              await sp.web.lists.getById("626f9e40-527d-472a-80a9-4ee6640ff524").items.add({
                HomeId: home.dtaHome.length === 1 ? home.dtaHome[0].key : null,
                Self_Funded_Occ: home.dtaTotalOccupancySF,
                LA_Funded_Occ: home.dtaTotalOccupancyLA,
                Self_Funded_Rate: home.dtaRoomRatesSF,
                LA_Funded_Rate: home.dtaRoomRatesLA,
                Cost_Of_Care: home.dtaCostOfCare,
                Additional_Details: home.dtaAdditionalDetails,
                TUPE: home.dtaTUPE,
                Price_Submission: home.dtaPriceSubmissionRequired,
                totalOccupancy: home.dtaTotalOccupancy,
                Method_Statements: home.dtaMethodStatementsRequired,
                TUPE_Notes: home.dtaTUPEAdditionalNotes,
                Price_Submission_Notes: home.dtaPriceAdditionalNotes,
                Method_Statements_Notes: home.dtaMethodAdditionalNotes,
                TenderId: returnedVal.data.Id,
                HomeOrder: index,
                proposed_weekly_fee: home.dtaWeeklyFee,
                rates_options: JSON.stringify(home.dtaRoomRatesList)
              });
              promiseCount++;
              if(promiseCount === dtaHomeDetails.length) {
                resolve();
              }
            });
          });
          if(!passive) {
            window.location.href = `${window.location.href}?EditModeId=${returnedVal.data.Id}`;
          }
          editMode.current = true;
          currentID.current = returnedVal.data.Id;
          formData.current = [returnedVal.data];
          alert("gothere");
          history.pushState({
            id: 'Edit Mode'
          }, 'Edit Mode', `${window.location.href}?EditModeId=${returnedVal.data.Id}`);
          setSaving(false);
        });
      });

    } else {
      // edit mode
      sp.web.lists.getById("eff099e9-fa68-4675-9712-54ff99e68385").items.getById(currentID.current).update({
        Date_x0020_Received: dtaSummary.dtaDateRecieved,
        CommissionersId: dtaSummary.dtaCommissioners.length > 0 ? dtaSummary.dtaCommissioners[0].key : null,
        contracting_companyId: dtaSummary.dtaContractingCompany.length > 0 ? dtaSummary.dtaContractingCompany[0].key : null,
        Opportunity_x0020_Type: dtaSummary.dtaOpportunityTypeKey,
        Title: dtaSummary.dtaOpportunityName,
        Opportunity_x0020_Summary: dtaSummary.dtaOpportunitySummary,
        Clarification_x0020_Deadline: dtaSummary.dtaClarificationDeadline,
        Submission_x0020_Deadline: dtaSummary.dtaSubmissionDeadline,
        Contract_x0020_Start_x0020_Date: dtaSummary.dtaContractStartDate,
        Contract_x0020_End_x0020_Date: dtaSummary.dtaContractExpiryDate,
        workflow_status: dtaSummary.dtaOpportunityStatusKey,
        Summary_Statement: dtaSummary.dtaSummaryStatement,
  
        // Background and exit
        Notable_x0020_Contract_x0020_Ter: dtaBackgroundExit.dtaContractTerms,
        Care_x0020_Specification: dtaBackgroundExit.dtaCareSpec,
        Exit_x0020_Arrangements_x0020_Re: dtaBackgroundExit.dtaExitArrangements,
        Termination_x0020_Clause: dtaBackgroundExit.dtaTerminationClause,
        Detail_x0020_Possible_x0020_Exit: dtaBackgroundExit.dtaExitIssues,
        section1complete: dtaBackgroundExit.dtaComplete,
  
        // Stakeholder review
        date_sent: dtaStakeholderReview.dtaDateSent,
        response_deadline: dtaStakeholderReview.dtaResponseDeadline,
        reminders: dtaStakeholderReview.dtaReminders,
        days: dtaStakeholderReview.dtaDays,
        head_comm_analysisId: Head_Comm_Analysis,
        head_salesId: Head_Of_Sales,
        regional_directorId: Regional_Director,
        regional_managerId: Regional_Manager,
        head_comm_analysis_endorsed: dtaStakeholderReview.dtaHeadCommAnalysisEndorsement,
        head_sales_endorsed: dtaStakeholderReview.dtaHeadOfSalesEndorsement,
        regional_director_endorsed: dtaStakeholderReview.dtaRegionalDirectorEndorsement,
        regional_manager_endorsed: dtaStakeholderReview.dtaRegionalManagerEndorsement,
        head_comm_analysis_notes: dtaStakeholderReview.dtaHeadCommAnalysisNotes,
        head_sales_notes: dtaStakeholderReview.dtaHeadOfSalesNotes,
        regional_director_notes: dtaStakeholderReview.dtaRegionalDirectorNotes,
        regional_manager_notes: dtaStakeholderReview.dtaRegionalManagerNotes,
        section2complete: dtaStakeholderReview.dtaComplete,

        // Director Review
  
        Sustainability_Matrix: dtaDirectorReview.dtaSustainabilityMatrix,
        supporting_views_input: dtaDirectorReview.dtaSupportingViewsInput,
        Recommendation: dtaDirectorReview.dtaFinanceKey,
        director_review_notes: dtaDirectorReview.dtaReccomendation,
        group_directorId: Group_Director,
        business_development_directorId: Business_Development,
        director_operationsId: Director_Operations,
        director_financeId: Director_Finance,
        group_director_authorised: dtaDirectorReview.dtaGroupDirectorAuthorised,
        business_development_authorised: dtaDirectorReview.dtaDirectorBusinessDevAuthorised,
        director_operations_authorised: dtaDirectorReview.dtaDirectorOperationsAuthorised,
        director_finance_authorised: dtaDirectorReview.dtaDirectorFinanceAuthorised,
        group_director_notes: dtaDirectorReview.dtaGroupDirectorNotes,
        business_development_notes: dtaDirectorReview.dtaDirectorBusinessDevNotes,
        director_operations_notes: dtaDirectorReview.dtaDirectorOperationsNotes,
        director_finance_notes: dtaDirectorReview.dtaDirectorFinanceNotes,
        section3complete: dtaDirectorReview.dtaComplete,
        sendStakeholderEmails: sendStakeholderEmail.current,
        sendDirectorEmails: sendDirectorEmail.current,
        Director_Date_Sent: dtaDirectorReview.dtaDateSent,
        Director_Response_Deadline: dtaDirectorReview.dtaResponseDeadline,
        Director_Reminders: dtaDirectorReview.dtaDirectorReminders,
        Reminder_Interval: dtaDirectorReview.dtaReminderInterval
      }).then(async (returnedVal: IItemUpdateResult) => {
        // Logic for updating home details on other list
        let existingData = await sp.web.lists.getById("626f9e40-527d-472a-80a9-4ee6640ff524").items.filter(`TenderId eq ${currentID.current}`).getAll();
        // Find deleted records
        existingData.forEach(async (record) => {
          if(dtaHomeDetails.map(home => home.dtaPositionTag).indexOf(record.HomeOrder) === -1) {
            await sp.web.lists.getById("626f9e40-527d-472a-80a9-4ee6640ff524").items.getById(record.Id).delete();
          }
        });

        // Sync form data to list
        let count = 0;
        await new Promise((resolve) => {
          dtaHomeDetails.forEach(async (home, index) => {
          // if a home has a tag value of -1 it means that it was added during this session and will not exist on the list
          if(home.dtaPositionTag === -1) {
            // add new
            await sp.web.lists.getById("626f9e40-527d-472a-80a9-4ee6640ff524").items.add({
              HomeId: home.dtaHome.length === 1 ? home.dtaHome[0].key : null,
              Self_Funded_Occ: home.dtaTotalOccupancySF,
              LA_Funded_Occ: home.dtaTotalOccupancyLA,
              Self_Funded_Rate: home.dtaRoomRatesSF,
              LA_Funded_Rate: home.dtaRoomRatesLA,
              Cost_Of_Care: home.dtaCostOfCare,
              Additional_Details: home.dtaAdditionalDetails,
              TUPE: home.dtaTUPE,
              Price_Submission: home.dtaPriceSubmissionRequired,
              totalOccupancy: home.dtaTotalOccupancy,
              Method_Statements: home.dtaMethodStatementsRequired,
              TUPE_Notes: home.dtaTUPEAdditionalNotes,
              Price_Submission_Notes: home.dtaPriceAdditionalNotes,
              Method_Statements_Notes: home.dtaMethodAdditionalNotes,
              TenderId: currentID.current,
              proposed_weekly_fee: home.dtaWeeklyFee,
              HomeOrder: index,
              rates_options: JSON.stringify(home.dtaRoomRatesList)
            });
          } else {
            // Update an existing record
            let matchedRecord = existingData.filter((val) => val.HomeOrder === home.dtaPositionTag);
            if (matchedRecord.length > 1 || matchedRecord.length === 0) {
              alert("Error, there are duplicate or missing home entries!");
            }
            else {
              await sp.web.lists.getById("626f9e40-527d-472a-80a9-4ee6640ff524").items.getById(matchedRecord[0].Id).update({
                HomeId: home.dtaHome.length === 1 ? home.dtaHome[0].key : null,
                Self_Funded_Occ: home.dtaTotalOccupancySF,
                LA_Funded_Occ: home.dtaTotalOccupancyLA,
                Self_Funded_Rate: home.dtaRoomRatesSF,
                LA_Funded_Rate: home.dtaRoomRatesLA,
                Cost_Of_Care: home.dtaCostOfCare,
                Additional_Details: home.dtaAdditionalDetails,
                TUPE: home.dtaTUPE,
                Price_Submission: home.dtaPriceSubmissionRequired,
                totalOccupancy: home.dtaTotalOccupancy,
                Method_Statements: home.dtaMethodStatementsRequired,
                TUPE_Notes: home.dtaTUPEAdditionalNotes,
                Price_Submission_Notes: home.dtaPriceAdditionalNotes,
                Method_Statements_Notes: home.dtaMethodAdditionalNotes,
                proposed_weekly_fee: home.dtaWeeklyFee,
                TenderId: currentID.current,
                HomeOrder: index,
                rates_options: JSON.stringify(home.dtaRoomRatesList)
              });
            }
          }
          count = count + 1;
          if(count === dtaHomeDetails.length) {
            resolve();
          }
        });});
        if(!passive) {
          window.location.href = "https://sanctuarygroup.sharepoint.com.mcas.ms/sites/CARE-TEST-Contracts";
        }
        formData.current = [await returnedVal.item.get()];
        setSaving(false);
        console.log("Promise resolved");
      });
    }
  };
  const saveForm = async (passive?: boolean, pivot?: string, selectedTab?: number) => {
    // Check if it's in edit mode or not
    let status = null;
    if(checkMandatory()) {
      if(passive) {
        if(mismatchFound.current === null) {
          status = await checkMergeError();
          if(status === false) {
            setSaving(true);
            try {
              await saveFormCallback(passive);
              mismatchFound.current = null;
              if(pivot == "main") {
                setSelectedMainTab(selectedTab);
              } else {
                setSelectedTabOpportunity(selectedTab);
              }  
            }
            catch {
              setSaving(false);
              alert("An error has prevented the form from saving.");
            }
          }
        } else {
          setSaving(true);
          try {
            await saveFormCallback(passive);
            mismatchFound.current = null;
            if(pivot == "main") {
              setSelectedMainTab(selectedTab);
            } else {
              setSelectedTabOpportunity(selectedTab);
            }  
          }catch {
            setSaving(false);
            alert("An error has prevented the form from saving.");
          }
        }
      } else {
        if(mismatchFound.current === null) {
          try {
            status = await checkMergeError();
            if(status === false) {
              setSaving(true);
              mismatchFound.current = null;
              await saveFormCallback();
            }
          } catch {
            setSaving(false);
            alert("An error has prevented the form from saving.");
          }
        } else {
          try {
            setSaving(true);
            mismatchFound.current = null;
            await saveFormCallback();  
          } catch {
            setSaving(false);
            alert("An error has prevented the form from saving.");
          }
        }
      }
    }
  };

  const directorReview = () => {
    if(stakeholderReviewDone.current) {
      setareYouSureDirectors(true);
    } else {
      setAreYouSureStakeholders(true);
    }
  };

  const sendDirectorReview = () => {
    sendStakeholderEmail.current = true;
    saveForm();
  };

  const updateObject = (obj: IHomeDetails, index: number) => {
    let updatedHome = [...dtaHomeDetails];
    updatedHome[index] = obj;
    setDtaHomeDetails(updatedHome);
  };

  const updateHomeRate = (homeIndex: number, rateIndex: number, obj: IRoomRates) => {
    let updatedRate: IRoomRates[] = [...dtaHomeDetails[homeIndex].dtaRoomRatesList];
    updatedRate[rateIndex] = obj;
    let updatedHome = [...dtaHomeDetails];
    updatedHome[homeIndex].dtaRoomRatesList = updatedRate;
    setDtaHomeDetails(updatedHome);
  };

  const homeChanged = async (changedItem: ITag[], homeID: number) => {
    let updatedHome: IHomeDetails[] = [...dtaHomeDetails];
    // Look up the changed item and save the item into state
    if(changedItem.length === 1) {
      // Home has been added
      // Need to pull down total occupancy for the selected home
      let selectedOccupancy = await sp.web.lists.getById("43fb1e67-1e38-446d-876e-e83db983aa1b").items.filter(`Id eq ${changedItem[0].key}`).select("Title","Id","Total_x0020_Occupancy").get();
      updatedHome[homeID].dtaHome = changedItem;
      updatedHome[homeID].dtaTotalOccupancy = selectedOccupancy[0].Total_x0020_Occupancy;
      setDtaHomeDetails(updatedHome);
    } else {
      // Home has been removed
      updatedHome[homeID].dtaHome = [];
      updatedHome[homeID].dtaTotalOccupancy = 0;
      setDtaHomeDetails(updatedHome);
    }
  };

  const checkPercentage = (userInput: string, field: string, homeID: number) => {
    let num: number;
    console.log(userInput);
    let updatedHome: IHomeDetails[] = [...dtaHomeDetails];
    if(!(isNaN(Number(userInput)))) {
      // Valid entry

      num = Number(userInput);
      console.log(`num: ${num}, total occupancy: ${updatedHome[homeID].dtaTotalOccupancy}, field: ${field}`);
      if(num >= 0 && num <= updatedHome[homeID].dtaTotalOccupancy && field === "SF") {
        // Correct range
        // Make sure that it does not go over 100%
        if(num + Number(updatedHome[homeID].dtaTotalOccupancyLA) > updatedHome[homeID].dtaTotalOccupancy) {
          updatedHome[homeID].dtaTotalOccupancySF = String(updatedHome[homeID].dtaTotalOccupancy - Number(updatedHome[homeID].dtaTotalOccupancyLA));
          setDtaHomeDetails(updatedHome);
        } else {
          updatedHome[homeID].dtaTotalOccupancySF = String(num);
          setDtaHomeDetails(updatedHome);
        }
      } else if (num >= 0 && num <= updatedHome[homeID].dtaTotalOccupancy && field === "LA") {
        if(num + Number(dtaHomeDetails[homeID].dtaTotalOccupancySF) > updatedHome[homeID].dtaTotalOccupancy) {
          updatedHome[homeID].dtaTotalOccupancyLA = String(updatedHome[homeID].dtaTotalOccupancy - Number(dtaHomeDetails[homeID].dtaTotalOccupancySF));
          setDtaHomeDetails(updatedHome);
        } else {
          updatedHome[homeID].dtaTotalOccupancyLA = String(num);
          setDtaHomeDetails(updatedHome);
        }
      } else {
        setDtaHomeDetails(updatedHome);
      }
    } else {
      setDtaHomeDetails(updatedHome);
    }
  };
  const listContainsTagList = (tag: ITag, tagList?: ITag[]) => {
    if (!tagList || !tagList.length || tagList.length === 0) {
      return false;
    }
    return tagList.some(compareTag => compareTag.key === tag.key);
  };

  const filterSuggestedTags = (filterText: string, tagList: ITag[], newList: ITag[]): ITag[] => {
    if(filterText === " "){
      return newList;
    }
    else {
      return filterText
      ? newList.filter(
          tag => tag.name.toLowerCase().indexOf(filterText.toLowerCase()) === 0 && !listContainsTagList(tag, tagList),
        )
      : [];
    }
  };

  const getTextFromItem = (item: ITag) => item.name;

  const onRenderOption = (option: IDropdownOption): JSX.Element => {
    if(option.text === "Financial Risk Unsustainable Offer") {
      return (
        <div>
          <span className={styles.red}>{option.text}</span>
        </div>
      );
    } else if (option.text === "Financial Risk – Can be mitigated") {
      return (
        <div>
          <span className={styles.orange}>{option.text}</span>
        </div>
      );
    } else {
      return (
        <div>
          <span className={styles.green}>{option.text}</span>
        </div>
      );
    }
  };

  const onRenderTitle = (options: IDropdownOption[]): JSX.Element => {
    const option = options[0];
    if(option.text === "Financial Risk Unsustainable Offer") {
      return (
        <div>
          <span className={styles.red}>{option.text}</span>
        </div>
      );
    } else if (option.text === "Financial Risk – Can be mitigated") {
      return (
        <div>
          <span className={styles.orange}>{option.text}</span>
        </div>
      );
    } else {
      return (
        <div>
          <span className={styles.green}>{option.text}</span>
        </div>
      );
    }
  };
 
  const getListData = () => {
    console.log(dtaHomeDetails[0].dtaRoomRatesList);
    return dtaHomeDetails[0].dtaRoomRatesList;
  };

  // From https://developer.microsoft.com/en-us/fluentui#/controls/web/pickers
  React.useEffect(() => {
    // Run one time when the form is loaded
    // Pull down all the metadata for the contracts list
    // Use metadata to populate options
    const thing = sp.web.lists.getById("eff099e9-fa68-4675-9712-54ff99e68385");
    thing.get().then((val) => {
      console.log(val);
    });

    const list = sp.web.lists.getById("eff099e9-fa68-4675-9712-54ff99e68385");
    list.fields().then((listMetaData: any) => {
      listMetaData.forEach((listField)  => {
        switch(listField.StaticName) {
          case "workflow_status":
            setDtaSummary((lastVal) => ({...lastVal, opportunityStatusOptions: listField.Choices.map((choice):IComboBoxOption => ({key: String(choice), text: String(choice)}))}));
            break;
          case "Opportunity_x0020_Type":
            setDtaSummary((lastVal) => ({...lastVal, opportunityTypeOptions: listField.Choices.map((choice):IDropdownOption => ({key: String(choice), text: String(choice)}))}));
            break;
        }
      });
    });
    sp.web.lists.getById("43fb1e67-1e38-446d-876e-e83db983aa1b").items.select("Title","Id").getAll().then((Homedata) => {
      // Store ready to be used on the picker
      setHomesDataPicker(Homedata.map(dataItem => ({key: dataItem.Id, name: dataItem.Title})));
      sp.web.lists.getById("e1f1ba61-b242-4479-8c6a-f4d62b862f84").items.select("Title","Id").getAll().then((Commdata) => {
        // Store ready to be used on the picker
        setCommissionerDataPicker(Commdata.map(dataItem => ({key: dataItem.Id, name: dataItem.Title})));
        sp.web.lists.getById("7db44937-be71-4b5c-8e37-0dbca52793d4").items.select("Title","Id").getAll().then((Companydata) => {
          // Store ready to be used on the picker
          dependenciesLoaded.current = true;
          setcontractingCompanyPicker(Companydata.map(dataItem => ({key: dataItem.Id, name: dataItem.Title})));
        });
      });
    });
  },[]);

  React.useEffect(() => {
    if(dependenciesLoaded.current) {
      // The data from the pickers has been loaded and now the form can check if it needs to pull record data from SharePoint (Edit Mode etc)
      checkEditMode();
    }
  }, [contractingCompanyPicker]);

  const endorsedOptions: IDropdownOption[] = [
    {key: "Yes", text: "Yes"},
    {key: "No", text: "No"},
    {key: "", text: ""},
  ];

  const financeOptions: IDropdownOption[] = [
    {key: "Financial Risk Unsustainable Offer", text: "Financial Risk Unsustainable Offer"},
    {key: "Financial Risk – Can be mitigated", text: "Financial Risk – Can be mitigated"},
    {key: "Good Rates and Terms – Proceed to next Stage", text: "Good Rates and Terms – Proceed to next Stage"},
  ];

  const ratesOptions: IComboBoxOption[] = [
    {key: "Residential General", text: "Residential General"},
    {key: "Residential Dementia", text: "Residential Dementia"},
    {key: "Nursing General", text: "Nursing General"},
    {key: "Nursing Dementia", text: "Nursing Dementia"},
    {key: "Respite", text: "Respite"},
    {key: "D2A Beds", text: "D2A Beds"},
  ];

  const generateHomeRates = (item: IHomeDetails, homeIndex) => {
    // Takes in home which will be mapped against
    if(item.dtaRoomRatesList) {
      return item.dtaRoomRatesList.map((roomRate, rateIndex) => {
        return (
            <Stack key={`$STACK400-${rateIndex}`} horizontal tokens={stackTokens} className={styles.frmLayout}>
              <ComboBox label="Rate Category" allowFreeform options={ratesOptions} className={styles.rateOption} text={dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex].dtaRateType} onChange={(e, option,index,value) => {
                  option !== undefined ?
                  updateHomeRate(homeIndex, rateIndex, {...dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex], dtaRateType: String(option.key)})
                  : updateHomeRate(homeIndex, rateIndex, {...dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex], dtaRateType: value});
                }}/>
              <TextField key={`$TEXT600-${rateIndex}`} className={styles.rateChild} label="Self Funded" prefix="£" value={dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex].dtaSelfFunded} onChange={(e, newVal) => updateHomeRate(homeIndex, rateIndex, {...dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex], dtaSelfFunded: newVal})}/>
              <TextField key={`$TEXT700-${rateIndex}`} className={styles.rateChild} label="LA Funded" prefix="£" value={dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex].dtaLAFunded} onChange={(e, newVal) => updateHomeRate(homeIndex, rateIndex, {...dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex], dtaLAFunded: newVal})}/>
              <TextField key={`$TEXT800-${rateIndex}`} className={styles.rateChild} label="Cost of Care" prefix="£" value={dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex].dtaCostOfCare} onChange={(e, newVal) => updateHomeRate(homeIndex, rateIndex, {...dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex], dtaCostOfCare: newVal})}/>
              <TextField key={`$TEXT100-${rateIndex}`} className={styles.weeklyFee} label="Proposed Weekly Fee" disabled={!dtaHomeDetails[homeIndex].dtaPriceSubmissionRequired} prefix="£" value={dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex].dtaProposedWeeklyFee} onChange={(e, newVal) => updateHomeRate(homeIndex, rateIndex, {...dtaHomeDetails[homeIndex].dtaRoomRatesList[rateIndex], dtaProposedWeeklyFee: newVal})}/>
              <IconButton iconProps={{iconName: "Delete"}} key={`Icon100-${rateIndex}`} onClick={() => {
                 let updatedList = item.dtaRoomRatesList;
                 if(updatedList.length > 1) {
                   updatedList.splice(updatedList.indexOf(roomRate), 1);
                   updateObject({...dtaHomeDetails[homeIndex], dtaRoomRatesList: updatedList}, homeIndex);
                 }
              }} />
            </Stack>
        );
      });
    } else {
      return (<></>);
    }

  };

  const generateHomeForm = () => {
    return dtaHomeDetails.map((item, index) => {
      return(
      <Stack  key={`$STACK1-${index}`} tokens={stackTokens}>
        <Label key={`LABEL0-${index}`} className={styles.bold}>{`Home ${index + 1}`}</Label>
          <Stack  key={`$STACK2-${index}`}>
            <Label key={`$LABEL1-${index}`}>Enter Homes:</Label>
            <TagPicker
              key={`$TAG1-${index}`}
              selectedItems={dtaHomeDetails[index].dtaHome}
              removeButtonAriaLabel="Remove"
              onResolveSuggestions={(a,b) =>{ return filterSuggestedTags(a,b,homesDataPicker);}}
              getTextFromItem={getTextFromItem}
              pickerSuggestionsProps={{
                suggestionsHeaderText: 'Suggested Homes',
                noResultsFoundText: 'No home for that input',
              }}
              itemLimit={1}
              className={styles.frmLayout}
              onChange={(changedItem) => homeChanged(changedItem, index)}
            />
          </Stack>
          <Label  key={`$LABEL2-${index}`} className={styles.bold}>Occupancy</Label>
          <Stack  key={`$STACK3-${index}`} horizontal tokens={stackTokens} className={styles.frmLayout}>
            <TextField  key={`$TEXT1-${index}`} label="Total Beds" value={String(dtaHomeDetails[index].dtaTotalOccupancy)} className={styles.fiveRow} onChange={(e, newVal) => {if(!isNaN(Number(newVal))) {updateObject({...dtaHomeDetails[index], dtaTotalOccupancy: Number(newVal)}, index);}else{forceUpdate(1);}}}/>
            <TextField   key={`$TEXT2-${index}`} label="Current" readOnly value={String(Number(dtaHomeDetails[index].dtaTotalOccupancySF) + Number(dtaHomeDetails[index].dtaTotalOccupancyLA))}  className={styles.fiveRow}/>
            <TextField key={`$TEXT3-${index}`} label="Self funded" value={dtaHomeDetails[index].dtaTotalOccupancySF} onChange={(e,newVal) => checkPercentage(newVal, "SF",index)} className={styles.fiveRow}/>
            <TextField key={`$TEXT4-${index}`} label="LA Funded" value={dtaHomeDetails[index].dtaTotalOccupancyLA} onChange={(e,newVal) => checkPercentage(newVal, "LA", index)} className={styles.fiveRow}/>
            <TextField key={`$TEXT5-${index}`} readOnly label="Available" value={String(Number(dtaHomeDetails[index].dtaTotalOccupancy) - Number(dtaHomeDetails[index].dtaTotalOccupancyLA) - Number(dtaHomeDetails[index].dtaTotalOccupancySF))} className={styles.fiveRow}/>
          </Stack>
          <Stack key={`$STACK5-${index}`} horizontal tokens={stackTokens}>
            <Toggle key={`$TOGGLE1-${index}`}  label="TUPE?" onText="Yes" offText="No" className={styles.threeRow} onChange={(e,checked) => updateObject({...dtaHomeDetails[index], dtaTUPE: checked},index)} checked={dtaHomeDetails[index].dtaTUPE}/>
            <TextField key={`$TEXT10-${index}`} label="Additional Notes" multiline className={styles.frmLayout} value={dtaHomeDetails[index].dtaTUPEAdditionalNotes} onChange={(e, newVal) => updateObject({...dtaHomeDetails[index], dtaTUPEAdditionalNotes: newVal}, index)}/>
          </Stack>
          <Stack key={`$STACK6-${index}`} horizontal tokens={stackTokens}>
            <Toggle key={`$TOGGLE2-${index}`}  label="Price Submission Required?" className={styles.threeRow} onText="Yes" offText="No"  onChange={(e,checked) => updateObject({...dtaHomeDetails[index], dtaPriceSubmissionRequired: checked}, index)} checked={dtaHomeDetails[index].dtaPriceSubmissionRequired}/>
            <TextField key={`$TEXT11-${index}`} label="Additional Notes" multiline className={styles.frmLayout} value={dtaHomeDetails[index].dtaPriceAdditionalNotes} onChange={(e, newVal) => updateObject({...dtaHomeDetails[index], dtaPriceAdditionalNotes: newVal}, index)}/>
          </Stack>
          <Stack  key={`$STACK7-${index}`} horizontal tokens={stackTokens}>
            <Toggle  key={`$TOGGLE3-${index}`} label="Method Statements Required?" className={styles.threeRow} onText="Yes" offText="No"  onChange={(e,checked) => updateObject({...dtaHomeDetails[index], dtaMethodStatementsRequired: checked}, index)} checked={dtaHomeDetails[index].dtaMethodStatementsRequired}/>
            <TextField key={`$TEXT12-${index}`} label="Additional Notes" multiline className={styles.frmLayout} value={dtaHomeDetails[index].dtaMethodAdditionalNotes} onChange={(e, newVal) => updateObject({...dtaHomeDetails[index], dtaMethodAdditionalNotes: newVal}, index)}/>
          </Stack>
          <Label  key={`$LABEL3-${index}`} className={styles.bold}>Room Rates</Label>
          {generateHomeRates(item, index)}
          <DefaultButton text="Add New Rate" key={`BUTTON-10031-${index}`} onClick={() => updateObject({...dtaHomeDetails[index], dtaRoomRatesList: [...dtaHomeDetails[index].dtaRoomRatesList, {
            dtaCostOfCare: "", dtaLAFunded: "", dtaProposedWeeklyFee: "", dtaRateType: "", dtaSelfFunded: ""
          }]}, index)}/>
          <Label key={`$LABEL4-${index}`} className={styles.bold}>Additional Details</Label>
          <TextField key={`$TEXT9-${index}`} multiline className={styles.frmLayout} value={dtaHomeDetails[index].dtaAdditionalDetails} onChange={(e, newVal) => updateObject({...dtaHomeDetails[index], dtaAdditionalDetails: newVal}, index)}/>
        </Stack>
      );
    });
  };

  const newContract = () => {
    // Create a new contract based on data in the tender
    // Some testing to ensure that there isn't already a generated contract might be desirable
    let homeIDs: string[] = [];
    setDtaSummary({...dtaSummary, dtaOpportunityStatusKey: "Tender Successful"});
    dtaHomeDetails.forEach((home) => {
      if(home.dtaHome.length === 1) {
        homeIDs.push(home.dtaHome[0].key);
      }
    });
    sp.web.lists.getById("94D30779-5E35-4F70-B1B0-8F24FBFD16AB").items.add({
      Commissioning_x0020_AuthorityId: dtaSummary.dtaCommissioners.length === 1 ? dtaSummary.dtaCommissioners[0].key: null,
      Home_LookupId: {
        results: homeIDs
      },
      Title: dtaSummary.dtaOpportunityName
    }).then((returnedVal) => {
      sp.web.lists.getById("94D30779-5E35-4F70-B1B0-8F24FBFD16AB").items.getById(returnedVal.data.Id).update({
        Link_To_Edit: {
          '__metadata': { 'type': 'SP.FieldUrlValue' },
          'Description': 'Click to edit item',
          'Url': `https://sanctuarygroup.sharepoint.com.mcas.ms/sites/CARE-TEST-Contracts/SitePages/Contracts.aspx?EditModeId=${returnedVal.data.Id}`
        }
      }).then(() => {
        alert("New contract created successfully");
      });
    });

  };

  const listHomes = (): string => {
    let tempList: string[] = [];
    dtaHomeDetails.forEach((val) => {
      if(val.dtaHome.length === 1) {
        tempList.push(String(val.dtaHome[0].name));
      }
    });
    if(tempList.length > 0) {
      return tempList.join(", ");
    }
    else {
      return "No homes selected";
    }
  };
  const calculateTotalCostHomes = (): string => {
    let runningTotal: number = 0;
    if(dtaHomeDetails) {
      dtaHomeDetails.forEach((val) => {
        if(val.dtaRoomRatesList) {
          val.dtaRoomRatesList.forEach((rate) => {
            runningTotal += Number(rate.dtaCostOfCare);
          });
        }
      });
    }
    return String(runningTotal);
  };
  
  const calculateTotalWeeklyFee = (): string => {
    let runningTotal: number = 0;
    if(dtaHomeDetails) {
      dtaHomeDetails.forEach((val) => {
        if(val.dtaRoomRatesList) {
          val.dtaRoomRatesList.forEach((rate) => {
            runningTotal += Number(rate.dtaProposedWeeklyFee);
          });
        }
      });
    }
    return String(runningTotal);
  };

  const calculateIncome = (): string => {
    let totalcost = calculateTotalCostHomes();
    let totalfee = calculateTotalWeeklyFee();
    return String(Number(totalfee) - Number(totalcost));
  };

  return (
    <>
      <Dialog
        hidden={!areYouSureStakeholders}
        onDismiss={() => setAreYouSureStakeholders(false)}
        dialogContentProps={{
          type: DialogType.normal,
          title: 'Submission for Review',
          closeButtonAriaLabel: 'Close',
          subText: 'Are you sure you want to send approval emails to Stakeholders?',
        }}
      >
        <DialogFooter>
          <PrimaryButton onClick={()=> {sendStakeholderEmail.current = true; setAreYouSureStakeholders(false); saveForm();}} text="Send" />
          <DefaultButton onClick={() => setAreYouSureStakeholders(false)} text="Don't send" />
        </DialogFooter>
      </Dialog>
      <Dialog
        hidden={!areYouSureDirectors}
        onDismiss={() => setareYouSureDirectors(false)}
        dialogContentProps={{
          type: DialogType.normal,
          title: 'Submission for Review',
          closeButtonAriaLabel: 'Close',
          subText: 'Are you sure you want to send approval emails to Directors?',
        }}
      >
        <DialogFooter>
          <PrimaryButton onClick={()=> {contract_team_email_Status.current = "Sending"; setareYouSureDirectors(false); saveForm();}} text="Send" />
          <DefaultButton onClick={() => setareYouSureDirectors(false)} text="Don't send" />
        </DialogFooter>
      </Dialog>

    <Dialog hidden={formLoaded}>
       <Spinner label="Loading Record"/>
    </Dialog>
    <Dialog hidden={!saving}>
       <Spinner label="Saving"/>
    </Dialog>

    <Pivot selectedKey={String(selectedMainTab)} onLinkClick={(item) => {saveForm(true, "main", Number(item.props.itemKey));}} linkFormat={PivotLinkFormat.tabs} >
       <PivotItem
       itemKey = "0"
        headerText="Opportunity"
        onRenderItemLink={(p,r) => OpportunityCompleteRenderer(p,r,"Opportunity")}
        headerButtonProps={{
          'data-order': 1,
          'data-title': 'Summary',
          'className' : styles.pivotControl
        }}
      >
        <Pivot selectedKey={String(selectedTabOpportunity)} onLinkClick={(item) => {saveForm(true, "opportunity",Number(item.props.itemKey));}}>
          <PivotItem
          itemKey = "0"
            headerText="Summary"
            headerButtonProps={{
              'data-order': 1,
              'data-title': 'Summary',
            }}
          >
            <Stack tokens={stackTokens}>
              <Stack horizontal tokens={stackTokens} className={styles.frmLayout}>
                <DatePicker
                  strings={DayPickerStrings}
                  value={dtaSummary.dtaDateRecieved}
                  placeholder="Select a date"
                  label="Date Recieved"
                  onSelectDate={(newDate) => setDtaSummary({...dtaSummary, dtaDateRecieved: newDate})}
                  className={styles.twoRow}
                />
                <ComboBox label="Opportunity Status" selectedKey={dtaSummary.dtaOpportunityStatusKey} options={dtaSummary.opportunityStatusOptions} allowFreeform={false} onChange={(e,option) => setDtaSummary({...dtaSummary, dtaOpportunityStatusKey: String(option.key)}) } className={styles.frmLayout}/>
              </Stack>
              <Stack>
                <Label required>Enter Commissioning Authority:</Label>
                <TagPicker
                  selectedItems={dtaSummary.dtaCommissioners}
                  removeButtonAriaLabel="Remove"
                  onResolveSuggestions={(a,b) =>{ return filterSuggestedTags(a,b,CommissionerDataPicker);}}
                  getTextFromItem={getTextFromItem}
                  pickerSuggestionsProps={{
                    suggestionsHeaderText: 'Suggested Commissioners',
                    noResultsFoundText: 'No commissioner for that input',
                  }}
                  itemLimit={1}
                  className={styles.frmLayout}
                  onChange={(changedItem) => setDtaSummary({...dtaSummary, dtaCommissioners: changedItem})}
              />
              </Stack>
              <Stack>
                <Label required>Enter Contracting Company:</Label>
                <TagPicker
                  selectedItems={dtaSummary.dtaContractingCompany}
                  removeButtonAriaLabel="Remove"
                  onResolveSuggestions={(a,b) =>{ return filterSuggestedTags(a,b,contractingCompanyPicker);}}
                  getTextFromItem={getTextFromItem}
                  pickerSuggestionsProps={{
                    suggestionsHeaderText: 'Suggested Contracting Company',
                    noResultsFoundText: 'No contracting company for that input',
                  }}
                  itemLimit={1}
                  className={styles.frmLayout}
                  onChange={(changedItem) => setDtaSummary({...dtaSummary, dtaContractingCompany: changedItem})}
                />
              </Stack>
              <TextField
                value={dtaSummary.dtaOpportunityName}
                onChange={(e,val)=> setDtaSummary({...dtaSummary, dtaOpportunityName: val})}
                label="Opportunity Name"
                required
                className={styles.frmLayout}/>
              <Dropdown  options={dtaSummary.opportunityTypeOptions} label="Opportunity Type" onChange={(e,option) => setDtaSummary({...dtaSummary, dtaOpportunityTypeKey: String(option.key)})} selectedKey={dtaSummary.dtaOpportunityTypeKey}/>
              <Stack>
                <Label required>Opportunity Summary</Label>
                <TextField
                  multiline
                  onChange={(e,newValue: string) => {setDtaSummary({...dtaSummary, dtaOpportunitySummary: newValue}); return newValue;}}
                  value={dtaSummary.dtaOpportunitySummary}
                  className={styles.frmLayout}
                  />
              </Stack>
                <Stack horizontal tokens={stackTokens}>
                  <DatePicker
                    strings={DayPickerStrings}
                    value={dtaSummary.dtaClarificationDeadline}
                    placeholder="Select a date"
                    label="Clarification Deadline"
                    onSelectDate={(newDate) => setDtaSummary({...dtaSummary, dtaClarificationDeadline: newDate})}
                    className={styles.twoRow}
                  />
                  <DatePicker
                    strings={DayPickerStrings}
                    value={dtaSummary.dtaSubmissionDeadline}
                    placeholder="Select a date"
                    label="Submission Deadline"
                    onSelectDate={(newDate) => setDtaSummary({...dtaSummary, dtaSubmissionDeadline: newDate})}
                    className={styles.twoRow}
                  />
              </Stack>
              <Stack horizontal tokens={stackTokens}>
                <DatePicker
                  strings={DayPickerStrings}
                  value={dtaSummary.dtaContractStartDate}
                  placeholder="Select a date"
                  label="Contract Start Date"
                  onSelectDate={(newDate) => setDtaSummary({...dtaSummary, dtaContractStartDate: newDate})}
                  className={styles.twoRow}
                />
                <DatePicker
                  strings={DayPickerStrings}
                  value={dtaSummary.dtaContractExpiryDate}
                  placeholder="Select a date"
                  label="Contract Expiry Date"
                  onSelectDate={(newDate) => setDtaSummary({...dtaSummary, dtaContractExpiryDate: newDate})}
                  className={styles.twoRow}
                />
              </Stack>
              <PrimaryButton text="Save Form" className={styles.center} onClick={() => saveForm()}/>
            </Stack>
          </PivotItem>
          <PivotItem
          itemKey = "1"
            headerText="Financial Information"
            headerButtonProps={{
              'data-order': 1,
              'data-title': 'Financial Information'
            }}
          >
            <Stack tokens={stackTokens}>
              <Label className={styles.boldTitle}>Home Summary</Label>
              <Label className={styles.boldFinancial}>This Opportunity relates to {dtaHomeDetails.length} Homes.</Label>
              <Label className={styles.boldFinancial}>These are: {listHomes()}</Label>
              <Label className={styles.boldFinancial}>Total Cost of Care: £{calculateTotalCostHomes()}, Total Weekly Fee: £{calculateTotalWeeklyFee()}, Generating: £{calculateIncome()}</Label>
              <Stack>
                <Label className={styles.boldFinancial}>Summary Statement</Label>
                <TextField value={dtaSummary.dtaSummaryStatement} onChange={(ev,newVal) => setDtaSummary({...dtaSummary, dtaSummaryStatement: newVal})} multiline/>
              </Stack>
              {generateHomeForm()}
              <PrimaryButton text="Add New" className={styles.center} onClick={newHome}/>
              <PrimaryButton text="Remove Last" className={styles.center} onClick={removeHome}/>
              <PrimaryButton text="Save Form" className={styles.center} onClick={() => saveForm()}/>
            </Stack>
          </PivotItem>
          <PivotItem
          itemKey = "2"
            headerText="Terms and Termination"
            headerButtonProps={{
              'data-order': 2,
              'data-title': 'Terms and Termination'
            }}
          >
            <Stack tokens={stackTokens}>
              <TextField label="Notable Contract Terms" multiline value={dtaBackgroundExit.dtaContractTerms} onChange={(e,newVal) => setDtaBackgroundExit({...dtaBackgroundExit, dtaContractTerms: newVal})} className={styles.frmLayout}/>
              <TextField label="Care Specification" multiline value={dtaBackgroundExit.dtaCareSpec} onChange={(e,newVal) => setDtaBackgroundExit({...dtaBackgroundExit, dtaCareSpec: newVal})} className={styles.frmLayout}/>
              <Stack horizontal tokens={stackTokens}>
                <Toggle label="Exit Arrangements Required?" className={styles.threeRow} onText="Yes" offText="No"  onChange={(e,checked) => setDtaBackgroundExit({...dtaBackgroundExit, dtaExitArrangements: checked})} checked={dtaBackgroundExit.dtaExitArrangements}/>
                <TextField label="Termination Clause" multiline className={styles.frmLayout} value={dtaBackgroundExit.dtaTerminationClause} onChange={(e, newVal) => setDtaBackgroundExit({...dtaBackgroundExit, dtaTerminationClause: newVal})}/>
              </Stack>
              <TextField label="Detail Possible Exit Issues" multiline value={dtaBackgroundExit.dtaExitIssues} onChange={(e,newVal) => setDtaBackgroundExit({...dtaBackgroundExit, dtaExitIssues: newVal})} className={styles.frmLayout}/>
              <Stack horizontal tokens={stackTokens}>
                <PrimaryButton text={dtaBackgroundExit.dtaComplete ? "Mark as Incomplete": "Mark as Complete"}  className={styles.twoRow} onClick={() => setDtaBackgroundExit({...dtaBackgroundExit, dtaComplete: !dtaBackgroundExit.dtaComplete})}/>
                <PrimaryButton text="Save Form" className={styles.twoRow} onClick={() => saveForm()}/>
              </Stack>
            </Stack>
          </PivotItem>
      </Pivot>
      </PivotItem>
        <PivotItem
        itemKey = "1"
          headerText="Stakeholder Review"
          headerButtonProps={{
            'data-order': 1,
            'data-title': 'Summary'
          }}
          onRenderItemLink={(p,r) => OpportunityCompleteRenderer(p,r,"Stakeholder")}
        >
          <Stack tokens={stackTokens}>
            <Stack horizontal tokens={stackTokens}>
              <DatePicker
                strings={DayPickerStrings}
                value={dtaStakeholderReview.dtaDateSent}
                placeholder="Select a date"
                label="Date Sent"
                onSelectDate={(newDate) => setDtaStakeholderReview({...dtaStakeholderReview, dtaDateSent: newDate})}
                className={styles.fourRow}
              />
              <DatePicker
                strings={DayPickerStrings}
                value={dtaStakeholderReview.dtaResponseDeadline}
                placeholder="Select a date"
                label="Response Deadline"
                onSelectDate={(newDate) => setDtaStakeholderReview({...dtaStakeholderReview, dtaResponseDeadline: newDate})}
                className={styles.fourRow}
              />
              <Toggle label="Reminders" className={styles.fourRowCentre} onText="On" offText="Off"  onChange={(e,checked) => setDtaStakeholderReview({...dtaStakeholderReview, dtaReminders: checked})} checked={dtaStakeholderReview.dtaReminders}/>
              <TextField label="Reminder Interval (Days)" value={dtaStakeholderReview.dtaDays} onChange={(e,newVal) => setDtaStakeholderReview({...dtaStakeholderReview, dtaDays: newVal})} className={styles.fourRow}/>
            </Stack>
            <Stack horizontal tokens={stackTokens} >
              <PeoplePicker
                titleText="Head of Commercial Analysis"
                defaultSelectedUsers={dtaStakeholderReview.dtaHeadCommAnalysis.map((data) => data.secondaryText)}
                context={props.context}
                peoplePickerCntrlclassName={styles.threeRow}
                onChange={(people) => setDtaStakeholderReview({...dtaStakeholderReview, dtaHeadCommAnalysis: people})}
              />
              <Dropdown label="Endorsed?" options={endorsedOptions} selectedKey={dtaStakeholderReview.dtaHeadCommAnalysisEndorsement} onChange={(e,option) => setDtaStakeholderReview({...dtaStakeholderReview, dtaHeadCommAnalysisEndorsement: String(option.key)})} />
              <TextField multiline  label="Notes" placeholder="Please type comments here or type no comment" value={dtaStakeholderReview.dtaHeadCommAnalysisNotes} onChange={(e,newVal) => setDtaStakeholderReview({...dtaStakeholderReview, dtaHeadCommAnalysisNotes: newVal})} className={styles.frmLayout}/>
            </Stack>
            <Stack horizontal tokens={stackTokens} >
              <PeoplePicker
                titleText="Head of Sales and Marketing"
                defaultSelectedUsers={dtaStakeholderReview.dtaHeadOfSales.map((data) => data.secondaryText)}
                context={props.context}
                peoplePickerCntrlclassName={styles.threeRow}
                onChange={(people) => setDtaStakeholderReview({...dtaStakeholderReview, dtaHeadOfSales: people})}
              />
              <Dropdown label="Endorsed?"  options={endorsedOptions} selectedKey={dtaStakeholderReview.dtaHeadOfSalesEndorsement} onChange={(e,option) => setDtaStakeholderReview({...dtaStakeholderReview, dtaHeadOfSalesEndorsement: String(option.key)})} />
              <TextField multiline label="Notes" placeholder="Please type comments here or type no comment" value={dtaStakeholderReview.dtaHeadOfSalesNotes} onChange={(e,newVal) => setDtaStakeholderReview({...dtaStakeholderReview, dtaHeadOfSalesNotes: newVal})} className={styles.frmLayout}/>
            </Stack>
            <Stack horizontal tokens={stackTokens} >
              <PeoplePicker
                titleText="Regional Director"
                defaultSelectedUsers={dtaStakeholderReview.dtaRegionalDirector.map((data) => data.secondaryText)}
                context={props.context}
                peoplePickerCntrlclassName={styles.threeRow}
                onChange={(people) => setDtaStakeholderReview({...dtaStakeholderReview, dtaRegionalDirector: people})}
              />
              <Dropdown label="Endorsed?"  options={endorsedOptions} selectedKey={dtaStakeholderReview.dtaRegionalDirectorEndorsement} onChange={(e,option) => setDtaStakeholderReview({...dtaStakeholderReview, dtaRegionalDirectorEndorsement: String(option.key)})} />
              <TextField multiline label="Notes" placeholder="Please type comments here or type no comment" value={dtaStakeholderReview.dtaRegionalDirectorNotes} onChange={(e,newVal) => setDtaStakeholderReview({...dtaStakeholderReview, dtaRegionalDirectorNotes: newVal})} className={styles.frmLayout}/>
            </Stack>
            <Stack horizontal tokens={stackTokens}>
              <PeoplePicker
                titleText="Regional Manager"
                defaultSelectedUsers={dtaStakeholderReview.dtaRegionalManager.map((data) => data.secondaryText)}
                context={props.context}
                peoplePickerCntrlclassName={styles.threeRow}
                onChange={(people) => setDtaStakeholderReview({...dtaStakeholderReview, dtaRegionalManager: people})}
              />
              <Dropdown label="Endorsed?"  options={endorsedOptions} selectedKey={dtaStakeholderReview.dtaRegionalManagerEndorsement} onChange={(e,option) => setDtaStakeholderReview({...dtaStakeholderReview, dtaRegionalManagerEndorsement: String(option.key)})} />
              <TextField multiline label="Notes" placeholder="Please type comments here or type no comment" value={dtaStakeholderReview.dtaRegionalManagerNotes} onChange={(e,newVal) => setDtaStakeholderReview({...dtaStakeholderReview, dtaRegionalManagerNotes: newVal})} className={styles.frmLayout}/>
            </Stack>
            <Stack horizontal tokens={stackTokens}>
              <PrimaryButton text={dtaStakeholderReview.dtaComplete ? "Mark as Incomplete": "Mark as Complete"}  className={styles.threeRow} onClick={() => setDtaStakeholderReview({...dtaStakeholderReview, dtaComplete: !dtaStakeholderReview.dtaComplete})}/>`
              <PrimaryButton text="Save Form" className={styles.threeRow} onClick={() => saveForm()}/>
              <PrimaryButton className={styles.threeRow} onClick={directorReview} text={
                stakeholderReviewDone.current ?
                "Send for Director Review" :
                "Ready for Stakeholder Review"
                }/>
            </Stack>
          </Stack>
        </PivotItem>
        <PivotItem
        itemKey = "2"
          headerText="Director Review"
          onRenderItemLink={(p,r) => OpportunityCompleteRenderer(p,r,"DirectorReview")}
          headerButtonProps={{
            'data-order': 1,
            'data-title': 'Summary'
          }}
        >
          <Stack tokens={stackTokens}>
            <Stack>
              <Label>Commissioning Authority:</Label>
              <TagPicker
                selectedItems={dtaSummary.dtaCommissioners}
                removeButtonAriaLabel="Remove"
                onResolveSuggestions={(a,b) =>{ return filterSuggestedTags(a,b,[]);}}
                getTextFromItem={getTextFromItem}
                pickerSuggestionsProps={{
                  suggestionsHeaderText: 'Suggested Commissioners',
                  noResultsFoundText: 'No commissioner for that input',
                }}
                itemLimit={1}
                disabled
                className={styles.frmLayout}
            />
            </Stack>
            <Stack>
              <Label>Contracting Company:</Label>
              <TagPicker
                selectedItems={dtaSummary.dtaContractingCompany}
                removeButtonAriaLabel="Remove"
                onResolveSuggestions={(a,b) =>{ return filterSuggestedTags(a,b,contractingCompanyPicker);}}
                getTextFromItem={getTextFromItem}
                pickerSuggestionsProps={{
                  suggestionsHeaderText: 'Suggested Contracting Company',
                  noResultsFoundText: 'No contracting company for that input',
                }}
                itemLimit={1}
                className={styles.frmLayout}
                disabled
              />
            </Stack>
            <TextField value={dtaSummary.dtaOpportunityName}
              onChange={(e,val)=> setDtaSummary({...dtaSummary, dtaOpportunityName: val})}
              label="Opportunity Name"
              className={styles.frmLayout}
              disabled/>
             <Stack>
                <Label>Opportunity Summary</Label>
                <TextField
                  readOnly
                  onChange={(e,newValue: string) => {setDtaSummary({...dtaSummary, dtaOpportunitySummary: newValue}); return newValue;}}
                  value={dtaSummary.dtaOpportunitySummary}
                  multiline
                  disabled
                  className={styles.frmLayout}
                />
              </Stack>
            <TextField value={dtaDirectorReview.dtaSustainabilityMatrix}
            onChange={(e,val)=> setDtaDirectorReview({...dtaDirectorReview, dtaSustainabilityMatrix: val})}
            label="Sustainability Matrix"
            multiline
            className={styles.frmLayout}
            />
            <TextField value={dtaDirectorReview.dtaSupportingViewsInput}
            onChange={(e,val)=> setDtaDirectorReview({...dtaDirectorReview, dtaSupportingViewsInput: val})}
            label="Supporting Views Input"
            multiline
            className={styles.frmLayout}
            />
            <Dropdown options={financeOptions} selectedKey={dtaDirectorReview.dtaFinanceKey} label="Recommendation" onRenderTitle={onRenderTitle} onRenderOption={onRenderOption} onChange={(e,option) => {setDtaDirectorReview({...dtaDirectorReview, dtaFinanceKey: String(option.key)});}} />
            <TextField value={dtaDirectorReview.dtaReccomendation}
            onChange={(e,val)=> setDtaDirectorReview({...dtaDirectorReview, dtaReccomendation: val})}
            label="Notes"
            multiline
            className={styles.frmLayout}
            />
            <Stack horizontal tokens={stackTokens}>
              <DatePicker
                strings={DayPickerStrings}
                value={dtaDirectorReview.dtaDateSent}
                placeholder="Select a date"
                label="Date Sent"
                className={styles.fourRow}
                onSelectDate={(newDate) => setDtaDirectorReview({...dtaDirectorReview, dtaDateSent: newDate})}
              />
              <DatePicker
                strings={DayPickerStrings}
                value={dtaDirectorReview.dtaResponseDeadline}
                placeholder="Select a date"
                label="Response Deadline"
                className={styles.fourRow}
                onSelectDate={(newDate) => setDtaDirectorReview({...dtaDirectorReview, dtaResponseDeadline: newDate})}
              />
              <Toggle label="Reminders" onChange={(ev,checked) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorReminders: checked})} checked={dtaDirectorReview.dtaDirectorReminders} className={styles.fourRowReminders} onText="On" offText="Off" />
              <TextField label="Reminder Interval (days)" value={dtaDirectorReview.dtaReminderInterval} onChange={(ev,nv) => setDtaDirectorReview({...dtaDirectorReview, dtaReminderInterval: nv})} className={styles.fourRow}/>
            </Stack>
            <Stack horizontal tokens={stackTokens}>
              <PeoplePicker
                titleText="Group Director"
                defaultSelectedUsers={dtaDirectorReview.dtaGroupDirector.map((data) => data.secondaryText)}
                context={props.context}
                peoplePickerCntrlclassName={styles.twoRow}
                onChange={(people) => setDtaDirectorReview({...dtaDirectorReview, dtaGroupDirector: people})}
              />
              <Dropdown label="Authorised?" options={endorsedOptions} selectedKey={dtaDirectorReview.dtaGroupDirectorAuthorised} onChange={(e,option) => setDtaDirectorReview({...dtaDirectorReview, dtaGroupDirectorAuthorised: String(option.key)})} />
              <TextField label="Notes" multiline placeholder="Please type comments here or type no comment"  value={dtaDirectorReview.dtaGroupDirectorNotes} onChange={(e,newVal) => setDtaDirectorReview({...dtaDirectorReview, dtaGroupDirectorNotes: newVal})} className={styles.frmLayout}/>
            </Stack>
            <Stack horizontal tokens={stackTokens} >
              <PeoplePicker
                titleText="Director of Operations"
                defaultSelectedUsers={dtaDirectorReview.dtaDirectorOperations.map((data) => data.secondaryText)}
                context={props.context}
                peoplePickerCntrlclassName={styles.twoRow}
                onChange={(people) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorOperations: people})}
              />
              <Dropdown label="Authorised?"  options={endorsedOptions} selectedKey={dtaDirectorReview.dtaDirectorOperationsAuthorised} onChange={(e,option) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorOperationsAuthorised: String(option.key)})} />
              <TextField multiline label="Notes" placeholder="Please type comments here or type no comment" value={dtaDirectorReview.dtaDirectorOperationsNotes} onChange={(e,newVal) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorOperationsNotes: newVal})} className={styles.frmLayout}/>
            </Stack>
            <Stack horizontal tokens={stackTokens} >
              <PeoplePicker
                titleText="Business Development"
                defaultSelectedUsers={dtaDirectorReview.dtaDirectorBusinessDev.map((data) => data.secondaryText)}
                context={props.context}
                peoplePickerCntrlclassName={styles.twoRow}
                onChange={(people) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorBusinessDev: people})}
              />
              <Dropdown label="Authorised?"  options={endorsedOptions} selectedKey={dtaDirectorReview.dtaDirectorBusinessDevAuthorised} onChange={(e,option) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorBusinessDevAuthorised: String(option.key)})} />
              <TextField multiline label="Notes" placeholder="Please type comments here or type no comment" value={dtaDirectorReview.dtaDirectorBusinessDevNotes} onChange={(e,newVal) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorBusinessDevNotes: newVal})} className={styles.frmLayout}/>
            </Stack>
            <Stack horizontal tokens={stackTokens}>
              <PeoplePicker
                  titleText="Director of Finance"
                  defaultSelectedUsers={dtaDirectorReview.dtaDirectorFinance.map((data) => data.secondaryText)}
                  context={props.context}
                  peoplePickerCntrlclassName={styles.threeRow}
                  onChange={(people) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorFinance: people})}
                />
              <Dropdown label="Authorised?" options={endorsedOptions} selectedKey={dtaDirectorReview.dtaDirectorFinanceAuthorised} onChange={(e,option) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorFinanceAuthorised: String(option.key)})} />
              <TextField multiline label="Notes" placeholder="Please type comments here or type no comment" value={dtaDirectorReview.dtaDirectorFinanceNotes} onChange={(e,newVal) => setDtaDirectorReview({...dtaDirectorReview, dtaDirectorFinanceNotes: newVal})} className={styles.frmLayout}/>
            </Stack>
            <Stack horizontal tokens={stackTokens}>
              <PrimaryButton text="Save Form" className={styles.threeRow} onClick={() => saveForm()}/>
              <PrimaryButton text={dtaDirectorReview.dtaComplete ? "Mark as Incomplete": "Mark as Complete"}  className={styles.threeRow} onClick={() => setDtaDirectorReview({...dtaDirectorReview, dtaComplete: !dtaDirectorReview.dtaComplete})}/>
              <PrimaryButton text="Send for Director Review" className={styles.threeRow} onClick={sendDirectorReview}/>
            </Stack>
            <Stack horizontal tokens={stackTokens}>
            <PrimaryButton text="Tender Unsuccessful" disabled={!(
                dtaDirectorReview.dtaGroupDirectorAuthorised != "" &&
                dtaDirectorReview.dtaDirectorOperationsAuthorised != "" &&
                dtaDirectorReview.dtaDirectorBusinessDevAuthorised != "" &&
                dtaDirectorReview.dtaDirectorFinanceAuthorised != "")
              } className={styles.twoRow} onClick={() => {
                setDtaSummary({...dtaSummary, dtaOpportunityStatusKey: "Tender Unsuccessful"});
                saveForm();
              }}/>
              <PrimaryButton text="Tender Successful" disabled={!(
                dtaDirectorReview.dtaGroupDirectorAuthorised != "" &&
                dtaDirectorReview.dtaDirectorOperationsAuthorised != "" &&
                dtaDirectorReview.dtaDirectorBusinessDevAuthorised != "" &&
                dtaDirectorReview.dtaDirectorFinanceAuthorised != "")
              } className={styles.twoRow} onClick={newContract}/>
            </Stack>
          </Stack>
        </PivotItem>
    </Pivot>
    {editMode.current ? <ListView
      items={dtaDocumentList}
      compact={true}
      selectionMode={SelectionMode.none}
      viewFields={viewFields}
      iconFieldName='relativeURL'
      dragDropFiles={true}
      onDrop={fileDropped}
    />: null}
   </>
  );
};

export default CareOpportunityForm;