import { WebPartContext } from '@microsoft/sp-webpart-base';
export interface ICareOpportunityFormProps {
  description: string;
  context: WebPartContext;
}
