declare interface ICareOpportunityFormWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CareOpportunityFormWebPartStrings' {
  const strings: ICareOpportunityFormWebPartStrings;
  export = strings;
}
